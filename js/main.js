var keepWatching;
$(function(){
  
  if(Notification.permission !== "granted"){
    Notification.requestPermission();
  }
  
  $("#startWatcher").on("click",function(){
    startWatcher();
  });
  $("#stopWatcher").on("click",function(){
    stopWatcher();
  });
  $("#setNote").on("click",function(){
    api("setSession",{sesVar:"note",sesVarVal:$("#noteMsg").val()});
  });
  
});

api = function(f,d){
  $.ajax({
    method: "POST",
    url: "/api/index.php",
    data:{
      api: "021e0bfc2aeaf44d9a647b3865e716b4",
      f: f,
      d: d
    }
  }).done(function(e){
    if(e)
      console.log(JSON.parse(e));
  });
};

startWatcher = function(){
  keepWatching = true;
  watcherInterval = setInterval(function(){
    notificationWatcher();
  },5000);
};

stopWatcher = function(){
  keepWatching = false;
};

notificationWatcher = function(){
  $.ajax({
    method: "POST",
    url: "/api/index.php",
    data:{
      api: "021e0bfc2aeaf44d9a647b3865e716b4",
      f: "notify"
    }
  }).done(function(e){
    if(!keepWatching){
      clearInterval(watcherInterval);
    }
    if(e){
      msg = JSON.parse(e);
      if(msg.length > 0)
        if(Notification.permission === "granted"){
          new Notification("munsking's notes",{
            icon: "https://release.munsking.com/img/gooptoopAva.png",
            body: msg
          });
        }else{
          console.log(msg);
        }
    }
  });
};
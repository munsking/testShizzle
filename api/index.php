<?php
require './apiFunctions.php';
$api = new api;
$p = filter_input_array(INPUT_POST);
$s = filter_input_array(INPUT_SERVER);
if(isset($p["api"]) && $p["api"]==md5("munsking-".$s["HTTP_HOST"])){
  if(isset($p["f"]) && method_exists($api,$p["f"])){
    if(isset($p["d"])){
      $f=$p["f"];
      echo json_encode($api->$f($p["d"]));
    }else{
      $f=$p["f"];
      echo json_encode($api->$f());
    }
  }else{
    echo "function $p[f] doesn't exist";
  }
}else{
  echo "Read the api docs, maybe your key's missing?";
}

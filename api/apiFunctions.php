<?php
session_start();
class api{
  
  public function __construct() {
    return this;
  }
  
  public function getSession($data){
    return filter_var($_SESSION[$data["sesVar"]]);
  }
  
  public function setSession($data){
    return $_SESSION[$data["sesVar"]] = $data["sesVarVal"];
  }
  
  public function notify(){
    $note = new notifications();
    return $note->getNotification();
  }


  public function test($msg){
    return $msg;
  }
}

class notifications{
  
  public function __construct() {
    return this;
  }
  
  public function getNotification(){
    $api = new api;
    $note = $api->getSession(array("sesVar"=>"note"));
//    $api->setSession(array("sesVar"=>"note","sesVarVal"=>NULL));
    unset($_SESSION["note"]);
    return $note;
  }
  
}
  <footer class="footer">
    <div class="container">
      <p>
        Owned and created by munsking. <span class=”copy-left”>©</span>2016.<br />
        All images by me are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.<br />
        All code by me is licenced under <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPLv3</a>
      </p>
    </div>
  </footer>
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/main.js"></script>
  </body>
</html>